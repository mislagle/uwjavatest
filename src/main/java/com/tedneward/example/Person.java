package com.tedneward.example;

import java.beans.*;
import java.lang.reflect.Array;
import java.util.*;

public class Person implements Comparable{
  private int age;
  private String name;
  private double salary;
  private String ssn;
  private boolean propertyChangeFired = false;
  
  public Person() {
    this("", 0, 0.0d);
  }
  
  public Person(String n, int a, double s) {
    name = n;
    age = a;
    salary = s;
    ssn = "";
  }

  public int getAge() {
    return age;
  }
  public void setAge(int newAge) {
    if(newAge < 0){
      throw new IllegalArgumentException();
    } else {
      int old = age;
      age = newAge;

      this.pcs.firePropertyChange("age", old, newAge);
      propertyChangeFired = true;
    }
  }
  
  public String getName() {
    return name;
  }
  public void setName(String newName) {
    if(newName == null){
      throw new IllegalArgumentException();
    }else {
      String old = name;
      name = newName;

      this.pcs.firePropertyChange("name", old, newName);
      propertyChangeFired = true;
    }
  }
  
  public double getSalary() {
    return salary;
  }
  public void setSalary(double newSalary) {
    double old = salary;
    salary = newSalary;

    this.pcs.firePropertyChange("salary", old, newSalary);
    propertyChangeFired = true;
  }
  
  public String getSSN() {
    return ssn;
  }
  public void setSSN(String value) {
    String old = ssn;
    ssn = value;
    
    this.pcs.firePropertyChange("ssn", old, value);
    propertyChangeFired = true;
  }

  public boolean getPropertyChangeFired() {
    return propertyChangeFired;
  }

  public double calculateBonus() {
    return salary * 1.10;
  }
  
  public String becomeJudge() {
    return "The Honorable " + name;
  }
  
  public int timeWarp() {
    return age + 10;
  }

  public boolean equals(Object other) {
    if(other instanceof Person){
      Person p = (Person) other;
      return (this.name.equals(p.name) && this.age == p.age);
    } else {
      return false;
    }
  }

  @Override
  public String toString() {
    return "[Person name:" + name + " age:" + age + " salary:" + salary + "]";
  }

  public static ArrayList<Person> getNewardFamily(){
    ArrayList<Person> people = new ArrayList<Person>();
    people.add(new Person("Ted", 41, 250000));
    people.add(new Person("Charlotte", 43, 150000));
    people.add(new Person("Michael", 22, 10000));
    people.add(new Person("Matthew", 15, 0));

    return people;
  }

  public int compareTo (Object o){
    Person p1 = (Person) o;
    double difference = p1.getSalary() - this.salary;
    return (int)difference;
  }
  public static class AgeComparator implements Comparator {
    public AgeComparator() {}
    public int compare(Object o1, Object o2){
      Person p1 = (Person) o1;
      Person p2 = (Person) o2;
      return p1.getAge() - p2.getAge();
    }
  }
  // PropertyChangeListener support; you shouldn't need to change any of
  // these two methods or the field
  //
  private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
  public void addPropertyChangeListener(PropertyChangeListener listener) {
      this.pcs.addPropertyChangeListener(listener);
  }
  public void removePropertyChangeListener(PropertyChangeListener listener) {
      this.pcs.removePropertyChangeListener(listener);
  }
}
